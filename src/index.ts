import spotifyApi from "spotify-web-api-node";
import { RESTPostAPIWebhookWithTokenJSONBody } from "discord-api-types/v10";
import { env } from "node:process";
import { config } from "dotenv";
import express from "express";
import cors from "cors";
config();

const app = express();
app.use(cors());

const defaultSong: iSong = {
  name: "Nothing Playing",
  album: "Nothing Playing",
  albumUrl: "https://open.spotify.com",
  image: "/img/nosong.png",
  time: 0,
  duration: 3000,
};
const defaultData: iSpotifyData = {
  song: defaultSong,
};
interface iSpotifyData {
  userName?: string;
  url?: string;
  avatarUrl?: string;
  song: iSong;
}
interface iSong {
  name: string;
  artist?: string;
  artistUrl?: string;
  album: string;
  albumUrl: string;
  image: string;
  playlist?: string;
  playlistUrl?: string;
  time: number;
  duration: number;
}

let alertCooldown = false;

let ratelimited = false;

export let lastKnownSong: iSong = defaultSong;

const spotify = new spotifyApi({
  clientId: env.SPOTIFY_CLIENT_ID,
  clientSecret: env.SPOTIFY_CLIENT_SECRET,
  redirectUri: `http${env.HTTPS ? "s" : ""}://${env.SERVER}/auth`,
});

export const setRefreshToken = async (newToken: string) => {
  spotify.setRefreshToken(newToken);
  return await refreshAuth();
};

setInterval(async () => {
  console.log("a");
  const newTokens = (await spotify.refreshAccessToken()).body;
  spotify.setAccessToken(newTokens.access_token);
  if (newTokens.refresh_token) {
    spotify.setRefreshToken(newTokens.refresh_token);
  }
}, 1000 * 60 * 30);

async function refreshAuth(): Promise<boolean> {
  try {
    const oldAccessToken = spotify.getAccessToken();
    const oldRefreshToken = spotify.getRefreshToken()!;
    const authData = await spotify.authorizationCodeGrant(oldRefreshToken);
    spotify.setAccessToken(authData.body["access_token"]);
    spotify.setRefreshToken(authData.body["refresh_token"]);

    const me = (await spotify.getMe()).body;
    if (me.id !== env.SPOTIFY_OWNER_ID) {
      spotify.setAccessToken(oldAccessToken || "");
      spotify.setRefreshToken(oldRefreshToken);
      return false;
    }

    return true;
  } catch (error) {
    try {
      if (alertCooldown) return false;
      alertCooldown = true;
      setTimeout(() => (alertCooldown = false), 20000);
      const authUrl = spotify.createAuthorizeURL(
        ["user-read-currently-playing", "user-read-playback-state"],
        "asuhdiashdui"
      );

      console.log(authUrl);
      const data: RESTPostAPIWebhookWithTokenJSONBody = {
        content: `Spotify auth expired, please re-authenticate at ${authUrl}`,
      };
      const res = await fetch(env.DISCORD_WEBHOOK_URL!, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      return false;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
}

export async function getData(): Promise<iSpotifyData> {
  if (ratelimited) return { song: defaultSong };
  try {
    const userData = (await spotify.getMe()).body;
    const nowPlaying = (await spotify.getMyCurrentPlaybackState()).body;
    if (nowPlaying.item) {
      nowPlaying.item = nowPlaying.item as TrackObjectFull;
      lastKnownSong = {
        name: nowPlaying.item.name,
        artist: nowPlaying.item.artists[0].name,
        artistUrl: nowPlaying.item.artists[0].external_urls.spotify,
        album: nowPlaying.item.album.name,
        albumUrl: nowPlaying.item.album.external_urls.spotify,
        image: nowPlaying.item.album.images[0].url,
        time: nowPlaying.progress_ms!,
        duration: nowPlaying.item.duration_ms!,
      };
      if (nowPlaying.context?.type === "playlist") {
        const playlistData = (
          await spotify.getPlaylist(nowPlaying.context.uri.split(":")[2])
        ).body;
        lastKnownSong.playlist = playlistData.name;
        lastKnownSong.playlistUrl = playlistData.external_urls.spotify;
      }
    }
    const output: iSpotifyData = {
      song: lastKnownSong,
    };
    if (userData) {
      if (userData.images?.[0]) output.avatarUrl = userData.images[0].url;
      output.userName = userData.display_name || userData.id;
      output.url = userData.external_urls.spotify;
    }
    return output;
  } catch (error) {
    console.error(error);
    if ((error as WebapiRegularError).statusCode !== 401) console.error(error);
    if ((error as WebapiRegularError).statusCode === 429) {
      ratelimited = true;
      setTimeout(() => (ratelimited = false), 1000 * 60 * 5);
    } else {
      refreshAuth();
    }
    return {
      song: defaultSong,
    };
  }
}

app.get("/", async (req, res) => {
  res.json(await getData());
});

app.get("/auth", async (req, res) => {
  const success = await setRefreshToken(req.query.code as string);
  res.send(success ? "Success" : "Failure");
});

app.listen(env.PORT, () => {
  console.log(`Listening on port ${env.PORT}`);
});
